import React from "react";
import Navbar2 from "../Component/Navbar2";
const Home = () => {
  return (
    <div className="min-h-screen bg-grey-600">
      <Navbar2 />
      {/* Header */}
      <div className="max-md:m-[20px] max-md:mt-[25%] mt-[10%]">
        <h1 className="font-sans font-semibold racking-tight leading-[60px] w-[800px] max-md:w-full max-md:pr-[8%]  m-auto mt-[3%] text-[52px] text-center">
          Mulai proyek Anda berikutnya dengan Aplikasi{" "}
          <span className="text-cyan-600">TodoList</span>
        </h1>
        <h1 className=" max-md:w-full m-auto mt-[1%] text-[20px] text-center text-[#575757] pt-[10px]">
          Memudahkan dalam bekerja dan menata waktu
        </h1>
      </div>
      {/* End Header */}

      {/* Cart Todolist & Presensi */}
      <div className="p-10  grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 mx-auto  mt-[3%]">
        <div className="rounded overflow-hidden shadow-lg w-[300px] h-[450px] mx-auto mt-[10px]  md:mr-[-1px] ease-in duration-200 hover:bg-gray-100">
          <a href="/presensi">
            <img
              className="w-[256px] mx-auto mt-[20px]"
              src="https://images.ctfassets.net/dm4oa8qtogq0/4Ui07HT23bURcd6KsNE2JI/afa01e7b4f4688157aaa6948711f5957/template-gtd-review-icon.jpg?fm=webp&w=256"
              alt="Mountain"
            />
            <div className="px-6 py-4">
              <div className="font-bold text-center text-xl mb-2">
                Presensi Harian
              </div>
              <p className="text-gray-700  text-center">
                Membuat agar kamu semakin disiplin dalam bekerja dan
                mengingatkan ketika kamu lupa akan tugas
              </p>
            </div>
          </a>
        </div>
        <div className="rounded overflow-hidden shadow-lg  w-[300px] h-[450px] md:ml-[-1px] mt-[10px] mx-auto ease-in duration-200 hover:bg-gray-100">
          <a href="/todolist">
            <img
              className="w-[256px] mx-auto mt-[20px]"
              src="https://images.ctfassets.net/dm4oa8qtogq0/6TiTWjSetzQYlfaCmDNDLc/c4ce38de25e5b7229d3ba86c5ef33bba/template-meeting-agenda-icon.jpg?fm=webp&w=256"
              alt="River"
            />
            <div className="px-6 py-4">
              <div className="font-bold text-center text-xl mb-2">TodoList</div>
              <p className="text-gray-700 text-center">
                Membuat kamu lebih disiplin dalam bekerja dan agar kamu tidak
                malas malasan ketika tidak ada atasan
              </p>
            </div>
          </a>
        </div>
      </div>
      {/* End Cart Todolist & Presensi */}

      {/* Footer */}
      <div>
        <footer className="p-4  sm:p-6 bg-gray-300 mt-[10%] dark:bg-gray-900">
          <div className="md:flex md:justify-between">
            <div className="mb-6 md:mb-0">
              <a href="/home" className="flex items-center">
                <img
                  src="https://to-do-cdn.microsoft.com/static-assets/c87265a87f887380a04cf21925a56539b29364b51ae53e089c3ee2b2180148c6/icons/logo.png"
                  className="h-8 mr-3"
                  alt="FlowBite Logo"
                />
                <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">
                  TodoList
                </span>
              </a>
            </div>
            <div className="grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-">
              <div>
                <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">
                  Resources
                </h2>
                <ul className="text-gray-600 dark:text-gray-400">
                  <li className="mb-4">
                    <a
                      href="https://reactjs.org/"
                      target="_blank"
                      className="hover:underline"
                    >
                      React Js
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://tailwindcss.com/"
                      target="_blank"
                      className="hover:underline"
                    >
                      Tailwind CSS
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <hr className="my-6 border-gray-500 sm:mx-auto dark:border-gray-700 lg:my-8" />
          <div className="sm:flex sm:items-center sm:justify-between">
            <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
              © 2023{" "}
              <a href="/" target="_blank" className="hover:underline">
                TodoList™
              </a>
              . All Rights Reserved.
            </span>
          </div>
        </footer>
      </div>
      {/* End Footer */}
    </div>
  );
};

export default Home;
