import axios from "axios";
import React, { useEffect, useState } from "react";
import Navbar from "../Component/Navbar";
import Swal from "sweetalert2";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
const Profile = () => {
  // State
  const [user, setUser] = useState([]);
  const [profile1, setProfile] = useState([]);
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  // End State

  // Method
  const profile = async () => {
    await axios
      .get("http://localhost:4000/user/" + localStorage.getItem("userId"))
      .then((res) => {
        setUser(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    profile();
  }, []);
  const update = async (e) => {
    e.preventDefault();
    const fromData = new FormData();
    fromData.append("file", profile1);
    Swal.fire({
      title: "Apakah Ingin Di Update?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Di Update!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put("http://localhost:4000/user/photo/" + user.id, fromData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        Swal.fire({
          icon: "success",
          title: "Berhasil mengedit data",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };
  // End Method
  return (
    // Profile
    <div className=" bg-gray-200 dark:bg-gray-900 flex flex-wrap items-center justify-center">
      {/* Navbar */}
      <Navbar />
      {/* End Navbar */}
      {/* Card Profile */}
      <div className=" max-w-lg bg-white rounded dark:bg-gray-800 shadow-lg transform duration-200 easy-in-out m-12 mt-[100px]">
        {/* Background */}
        <div className="h-2/4 sm:h-64 overflow-hidden">
          <img
            className="w-full rounded-t"
            src={!user.background ? "backgorund.avif" : user.background}
          />
        </div>
        {/* End Background */}
        {/* Foto Profile */}
        <div className="flex justify-start px-5 -mt-12 mb-5">
          <span clspanss="block relative h-32 w-32">
            <img
              src={!user.foto ? "1-min.png" : user.foto}
              className="mx-auto object-cover rounded-full h-24 w-24 bg-white p-1"
            />
            <div onClick={handleShow} className="relative mt-[-26px] ml-[11px]">
              <i className="fa-solid fa-camera text-gray-100 bg-gray-700 px-7 py-0.5 rounded-bl-full rounded-br-full"></i>
            </div>
          </span>
        </div>
        {/* End Foto Profile */}
        {/* Data User */}
        <div className="">
          <div className="px-7 mb-8">
            <h2 className="text-3xl font-bold text-cyan-600 dark:text-gray-300">
              {user.username}
            </h2>
            <p className="text-gray-400 mt-2 dark:text-gray-400">
              {user.email}
            </p>
            <p className="text-gray-500 mt-2 dark:text-gray-400">
              {!user.telepon ? "No Telepon Tidak Terdaftar" : user.telepon}
            </p>
            <p className="mt-2 text-gray-600 dark:text-gray-300">
              {!user.alamat ? "Alamat Tidak Terdaftar" : user.alamat}
            </p>
            <a href="/edit-profile">
              <div className="justify-center px-4 py-2 cursor-pointer bg-gray-900 max-w-max mx-auto mt-8 rounded-lg text-gray-300 hover:bg-gray-700 hover:text-gray-100 dark:bg-gray-700 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-gray-200">
                Edit Profile
              </div>
            </a>
          </div>
        </div>
        {/* End Data user */}
      </div>
      {/* End Card Profile */}
      <Modal show={show} onHide={handleClose}>
        <Form className="" onSubmit={update}>
          <Modal.Header className="bg-gray-700 text-white" closeButton>
            <Modal.Title>Edit Photo Profile</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="judul">Profile: </Form.Label>
              <br />
              <Form.Control
                required
                type="file"
                onChange={(e) => setProfile(e.target.files[0])}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="mx-1  border-transparent bg-red-600 hover:bg-white hover:border-red-600 hover:text-red-600"
              variant="danger"
              onClick={handleClose}
            >
              Close
            </Button>
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
            >
              Save Changes
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </div>
    //End Profile
  );
};

export default Profile;
