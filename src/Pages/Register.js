import axios from "axios";
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

const Login = () => {
  // State
  const history = useHistory();
  const [nama, setNama] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [telepon, setTelepon] = useState("");
  const [alamat, setAlamat] = useState("");
  const [validasi, setValidasi] = useState("");

  // End State

  // Method
  const register = async (e) => {
    e.preventDefault();

    try {
      if (password === validasi) {
        const response = await axios.post(
          "http://localhost:4000/user/sign-up" ,{
            username:nama,
            password:password,
            email:email,
            telepon:telepon,
            alamat:alamat,
            role:"USER"
          }
        );
        if (response.status === 200) {
          Swal.fire({
            icon: "success",
            title: "Berhasil Registrasi",
            showConfirmButton: false,
            timer: 1500,
          });
          history.push("/");
        } else {
            Swal.fire({
                icon: "error",
                title: "Maaf email sudah digunakan",
                showConfirmButton: false,
                timer: 1500,
              });
        }
      } else {
        Swal.fire({
          icon: "error",
          title: "Maaf password tidak sama",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    } catch (err) {
      console.log(err);
    }
  };
  // End Method
  return (
    <div className="">
      {/* Register */}
      <div className="flex flex-col items-center min-h-screen pt-6 sm:justify-center sm:pt-0 bg-sky-100  max-md:h-[140vh]">
        {/* Icon */}
        <div>
          <a href="/">
            <h3 className="text-4xl font-bold text-sky-600">TodoList</h3>
            <img
              className="w-[100px] mt-[10px] mx-[auto]"
              src="https://to-do-cdn.microsoft.com/static-assets/c87265a87f887380a04cf21925a56539b29364b51ae53e089c3ee2b2180148c6/icons/logo.png"
              alt=""
            />
          </a>
        </div>
        {/* End Icon */}

        {/* Form Register */}
        <div className="w-full px-6 py-4 mt-6 overflow-hidden bg-grey-400 shadow-md sm:max-w-md sm:rounded-lg">
          <form onSubmit={register}>
            <div>
              <label
                htmlFor="name"
                className="block text-sm font-medium text-gray-700 undefined"
              >
                Nama
              </label>
              <div className="flex flex-col items-start">
                <input
                  type="text"
                  name="nama"
                  onChange={(e) => setNama(e.target.value)}
                  className="block p-[10px] w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                />
              </div>
            </div>
            <div className="mt-4">
              <label
                htmlFor="email"
                className="block text-sm font-medium text-gray-700 undefined"
              >
                Email
              </label>
              <div className="flex flex-col items-start">
                <input
                  onChange={(e) => setEmail(e.target.value)}
                  id="email"
                  required
                  type="email"
                  name="email"
                  className="block w-full p-[10px] mt-1 border-gray-800 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                />
              </div>
            </div>
            <div className="mt-4">
              <label
                htmlFor="password"
                className="block text-sm font-medium text-gray-700 undefined"
              >
                Password
              </label>
              <div className="flex flex-col items-start">
                <input
                  type="password"
                  name="password"
                  required
                  onChange={(e) => setPassword(e.target.value)}
                  className="block p-[10px] w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                />
              </div>
            </div>
            <div className="mt-4">
              <label
                htmlFor="password_confirmation"
                className="block text-sm font-medium text-gray-700 undefined"
              >
                Confirmasi Password
              </label>
              <div className="flex flex-col items-start">
                <input
                  type="password"
                  name="password_confirmation"
                  onChange={(e) => setValidasi(e.target.value)}
                  className="block p-[10px] w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                />
              </div>
            </div>
            <div className="flex items-center justify-end mt-4">
              <a
                className="text-sm text-gray-600 underline hover:text-gray-900"
                href="/"
              >
                Sudah registrasi?
              </a>
              <button
                type="submit"
                className="inline-flex items-center px-4 py-2 ml-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-gray-900 border border-transparent rounded-md active:bg-gray-900 false"
              >
                Register
              </button>
            </div>
          </form>
        </div>
        {/* End Form Regiter */}
      </div>
      {/* End Register */}
    </div>
  );
};

export default Login;
