import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
import axios from "axios";
const Login = () => {
  // State
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();
// End State

// Method
  const login = async (e) => {
    e.preventDefault();
    try {
      const { data, status } = await axios.post(
        "http://localhost:4000/user/sign-in",
        {
          email: email,
          password: password,
        }
      );
      //Jika response  status 200 ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title:
            "Login Berhasil!!" +
            "\n" +
            " Selamat Datang " +
            data.data.user.username,
          showConfirmButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.user.id);
        localStorage.setItem("nama", data.data.user.username);
        history.push("/home");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Email atau password tidak valid!",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };
  // End Method
  return (
    <div>
      <div className="flex flex-col items-center min-h-screen pt-6 sm:justify-center sm:pt-0 bg-sky-100  max-md:h-[100vh]">
        {/* Icon */}
        <div>
          <a href="/">
            <img
              className="w-[100px] mt-[10px] mx-[auto]"
              src="https://to-do-cdn.microsoft.com/static-assets/c87265a87f887380a04cf21925a56539b29364b51ae53e089c3ee2b2180148c6/icons/logo.png"
              alt=""
            />
            <h3 className="text-4xl font-bold text-sky-600">TodoList</h3>
          </a>
        </div>
        {/*End Icon */}

        {/*Form Login  */}
        <div className="w-full px-6 py-4 mt-6 overflow-hidden bg-grey-400 shadow-md sm:max-w-md sm:rounded-lg">
          <form onSubmit={login}>
            <div className="mt-4">
              <label
                htmlFor="email"
                className="block text-sm font-medium text-gray-700 undefined"
              >
                Email
              </label>
              <div className="flex flex-col items-start">
                <input
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  type="email"
                  name="email"
                  className="block w-full p-[10px] mt-1 border-gray-800 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                />
              </div>
            </div>
            <div className="mt-4">
              <label
                htmlFor="password"
                className="block text-sm font-medium text-gray-700 undefined"
              >
                Password
              </label>
              <div className="flex flex-col items-start">
                <input
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  type="password"
                  name="password"
                  className="block p-[10px] w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                />
              </div>
            </div>
            <div className="flex items-center justify-end mt-4">
              <a
                className="text-sm text-gray-600 underline hover:text-gray-900"
                href="/reg"
              >
                Belum registrasi?
              </a>
              <button
                type="submit"
                className="inline-flex items-center px-4 py-2 ml-4 text-xs font-semibold tracking-widest text-white uppercase transition duration-150 ease-in-out bg-gray-900 border border-transparent rounded-md active:bg-gray-900 false"
              >
                Login
              </button>
            </div>
          </form>
        </div>
        {/* End Form Login */}
      </div>
    </div>
  );
};

export default Login;
