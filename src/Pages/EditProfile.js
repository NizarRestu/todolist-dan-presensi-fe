import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
const EditProfile = () => {
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [telepon, setTelepon] = useState("");
  const [password, setPassword] = useState("");
  const [validasi, setValidasi] = useState("");
  const [profile1, setProfile] = useState([]);
  const [user, setUser] = useState([]);

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [show1, setShow1] = useState(false);
  const handleClose1 = () => setShow1(false);
  const handleShow1 = () => setShow1(true);

  const [show2, setShow2] = useState(false);
  const handleClose2 = () => setShow2(false);
  const handleShow2 = () => setShow2(true);

  const [show3, setShow3] = useState(false);
  const handleClose3 = () => setShow3(false);
  const handleShow3 = () => setShow3(true);

  const [show4, setShow4] = useState(false);
  const handleClose4 = () => setShow4(false);
  const handleShow4 = () => setShow4(true);
  const profile = async () => {
    await axios
      .get("http://localhost:4000/user/" + localStorage.getItem("userId"))
      .then((res) => {
        setUser(res.data.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    profile();
  }, []);
  const updateUsername = async (e) => {
    e.preventDefault();
    Swal.fire({
      title: "Apakah Ingin Di Update?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Di Update!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put("http://localhost:4000/user/username/" + user.id, {
          username: nama,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        Swal.fire({
          icon: "success",
          title: "Berhasil mengedit nama",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };
  const updateAlamat = async (e) => {
    e.preventDefault();
    Swal.fire({
      title: "Apakah Ingin Di Update?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Di Update!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put("http://localhost:4000/user/alamat/" + user.id, {
          alamat: alamat,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        Swal.fire({
          icon: "success",
          title: "Berhasil mengedit alamat",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };
  const updateTelepon = async (e) => {
    e.preventDefault();
    Swal.fire({
      title: "Apakah Ingin Di Update?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Di Update!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put("http://localhost:4000/user/telepon/" + user.id, {
          telepon: telepon,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        Swal.fire({
          icon: "success",
          title: "Berhasil mengedit no telepon",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };
  const updatePassword = async (e) => {
    e.preventDefault();
    if (validasi === password) {
      Swal.fire({
        title: "Apakah Ingin Di Update?",
        text: "",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Ya, Di Update!",
      }).then((result) => {
        if (result.isConfirmed) {
          axios.put("http://localhost:4000/user/password/" + user.id, {
            password: password,
          });
          setTimeout(() => {
            window.location.reload();
          }, 1500);
          Swal.fire({
            icon: "success",
            title: "Berhasil mengedit password",
            showConfirmButton: false,
            timer: 1500,
          });
        }
      });
    } else {
      Swal.fire({
        icon: "error",
        title: "Maaf password tidak sama",
        showConfirmButton: false,
        timer: 1500,
      });
    }
  };
  const update = async (e) => {
    e.preventDefault();
    const fromData = new FormData();
    fromData.append("file", profile1);
    Swal.fire({
      title: "Apakah Ingin Di Update?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Di Update!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put(
          "http://localhost:4000/user/background/" + user.id,
          fromData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        );
        setTimeout(() => {
          window.location.reload();
        }, 1500);
        Swal.fire({
          icon: "success",
          title: "Berhasil mengedit background",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };
  return (
    <div className=" w-full container mt-24  p-4 bg-white border border-gray-200 rounded-lg shadow sm:p-8 dark:bg-gray-800 dark:border-gray-700">
      <div className="flex items-center justify-between mb-4">
        <h5 className="text-xl font-bold leading-none text-gray-900 dark:text-white">
          Edit Profile
        </h5>
        <a
          href="/profile"
          className="text-sm font-medium text-blue-600 hover:underline dark:text-blue-500"
        >
          Kembali
        </a>
      </div>
      <div className="flow-root">
        <ul
          role="list"
          className="divide-y divide-gray-200 dark:divide-gray-700"
        >
          <li className="py-3 sm:py-4 ">
            <div className="flex items-center space-x-4">
              <div className="flex-shrink-0"></div>
              <div className="flex-1 min-w-0">
                <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                  Username
                </p>
                <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                  {user.username}
                </p>
              </div>
              <div
                onClick={handleShow}
                className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
              >
                <i className="fa-solid fa-arrow-right cursor-pointer"></i>
              </div>
            </div>
          </li>
          <li className="py-3 sm:py-4 ">
            <div className="flex items-center space-x-4">
              <div className="flex-shrink-0"></div>
              <div className="flex-1 min-w-0">
                <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                  Alamat
                </p>
                <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                  {user.alamat}
                </p>
              </div>
              <div
                onClick={handleShow1}
                className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
              >
                <i className="fa-solid fa-arrow-right cursor-pointer"></i>
              </div>
            </div>
          </li>
          <li className="py-3 sm:py-4">
            <div className="flex items-center space-x-4">
              <div className="flex-shrink-0"></div>
              <div className="flex-1 min-w-0">
                <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                  No Telepon
                </p>
                <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                  {user.telepon}
                </p>
              </div>
              <div
                onClick={handleShow2}
                className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
              >
                <i className="fa-solid fa-arrow-right cursor-pointer"></i>
              </div>
            </div>
          </li>
          <li className="py-3 sm:py-4">
            <div className="flex items-center space-x-4">
              <div className="flex-shrink-0"></div>
              <div className="flex-1 min-w-0">
                <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                  Password
                </p>
                <p className="text-sm text-gray-500 truncate dark:text-gray-400">
                  ********
                </p>
              </div>
              <div
                onClick={handleShow3}
                className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
              >
                <i className="fa-solid fa-arrow-right cursor-pointer"></i>
              </div>
            </div>
          </li>
          <li className="py-3 sm:py-4">
            <div className="flex items-center space-x-4">
              <div className="flex-shrink-0"></div>
              <div className="flex-1 min-w-0">
                <p className="text-sm font-medium text-gray-900 truncate dark:text-white">
                  Edit Backgorund
                </p>
              </div>
              <div
                onClick={handleShow4}
                className="inline-flex items-center text-base font-semibold text-gray-900 dark:text-white"
              >
                <i className="fa-solid fa-arrow-right cursor-pointer"></i>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <Modal show={show} onHide={handleClose}>
        <Form className="" onSubmit={updateUsername}>
          <Modal.Header className="bg-gray-700 text-white" closeButton>
            <Modal.Title>Edit Username</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="judul">Username: </Form.Label>
              <br />
              <Form.Control
                required
                type="text"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="mx-1  border-transparent bg-red-600 hover:bg-white hover:border-red-600 hover:text-red-600"
              variant="danger"
              onClick={handleClose}
            >
              Close
            </Button>
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
            >
              Save Changes
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      <Modal show={show1} onHide={handleClose1}>
        <Form className="" onSubmit={updateAlamat}>
          <Modal.Header className="bg-gray-700 text-white" closeButton>
            <Modal.Title>Edit Alamat</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="judul">Alamat: </Form.Label>
              <br />
              <Form.Control
                required
                type="text"
                value={alamat}
                onChange={(e) => setAlamat(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="mx-1  border-transparent bg-red-600 hover:bg-white hover:border-red-600 hover:text-red-600"
              variant="danger"
              onClick={handleClose1}
            >
              Close
            </Button>
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
            >
              Save Changes
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      <Modal show={show2} onHide={handleClose2}>
        <Form className="" onSubmit={updateTelepon}>
          <Modal.Header className="bg-gray-700 text-white" closeButton>
            <Modal.Title>Edit No Telepon</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="judul">No: </Form.Label>
              <br />
              <Form.Control
                required
                type="text"
                value={telepon}
                onChange={(e) => setTelepon(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="mx-1  border-transparent bg-red-600 hover:bg-white hover:border-red-600 hover:text-red-600"
              variant="danger"
              onClick={handleClose2}
            >
              Close
            </Button>
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
            >
              Save Changes
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      <Modal show={show3} onHide={handleClose3}>
        <Form className="" onSubmit={updatePassword}>
          <Modal.Header className="bg-gray-700 text-white" closeButton>
            <Modal.Title>Edit Password</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="judul">Password: </Form.Label>
              <br />
              <Form.Control
                required
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="judul">Konfirmasi Password: </Form.Label>
              <br />
              <Form.Control
                required
                type="password"
                value={validasi}
                onChange={(e) => setValidasi(e.target.value)}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="mx-1  border-transparent bg-red-600 hover:bg-white hover:border-red-600 hover:text-red-600"
              variant="danger"
              onClick={handleClose3}
            >
              Close
            </Button>
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
            >
              Save Changes
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      <Modal show={show4} onHide={handleClose4}>
        <Form className="" onSubmit={update}>
          <Modal.Header className="bg-gray-700 text-white" closeButton>
            <Modal.Title>Edit Background</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="judul">Background: </Form.Label>
              <br />
              <Form.Control
                required
                type="file"
                onChange={(e) => setProfile(e.target.files[0])}
              />
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button
              className="mx-1  border-transparent bg-red-600 hover:bg-white hover:border-red-600 hover:text-red-600"
              variant="danger"
              onClick={handleClose4}
            >
              Close
            </Button>
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
            >
              Save Changes
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </div>
  );
};

export default EditProfile;
