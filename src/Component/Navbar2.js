import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

const Navbar2 = () => {
  const history = useHistory();
  const [navbar, setNavbar] = useState(false);
  const logout = () => {
    Swal.fire({
      title: "Apakah Ingin Logout",
      text: "Kamu harus login lagi!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Logout!",
    }).then((result) => {
      if (result.isConfirmed) {
        setTimeout(() => {
          localStorage.clear();
          history.push("/");
          window.location.reload();
        }, 1500);
        Swal.fire({
          icon: "success",
          title: "Berhasil Logout",
          showConfirmButton: false,
          timer: 1500,
        });
      }
    });
  };
  return (
    <div>
      <nav className="w-full bg-white shadow fixed top-0 left-0 right-0 ">
        <div className="justify-between px-4 mx-auto lg:max-w-7xl md:items-center md:flex md:px-8">
          <div>
            <div className="flex items-center justify-between py-3 md:py-5 md:block">
              <a href="/home" className="flex items-center">
                <img
                  src="https://to-do-cdn.microsoft.com/static-assets/c87265a87f887380a04cf21925a56539b29364b51ae53e089c3ee2b2180148c6/icons/logo.png"
                  className="h-6 mr-3 sm:h-9"
                  alt="Flowbite Logo"
                />
                <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
                  TodoList
                </span>
              </a>
              <div className="md:hidden">
                <button
                  className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
                  onClick={() => setNavbar(!navbar)}
                >
                  {navbar ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6"
                      viewBox="0 0 20 20"
                      fill="currentColor"
                    >
                      <path
                        fillRule="evenodd"
                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                        clipRule="evenodd"
                      />
                    </svg>
                  ) : (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="w-6 h-6"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                      strokeWidth={2}
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M4 6h16M4 12h16M4 18h16"
                      />
                    </svg>
                  )}
                </button>
              </div>
            </div>
          </div>
          <div>
            <div
              className={`flex-1 justify-self-center text-center pb-3 mt-8 md:block md:pb-0 md:mt-0 ${
                navbar ? "block" : "hidden"
              }`}
            >
              <ul className="items-center justify-center space-y-8 md:flex md:space-x-6 cursor-pointer md:space-y-0">
                {localStorage.getItem("userId") !== null ? (
                  <>
                    <li className="text-gray-700 hover:text-blue-600 mt-2">
                      <a href="/profile">
                        <i className="fa-solid fa-user text-xl"></i>
                      </a>
                    </li>
                    <li className=" bg-gray-600 text-white px-3 py-2 mt-2 rounded hover:text-blue-600">
                      <a onClick={logout}>Logout</a>
                    </li>
                  </>
                ) : (
                  <li className="bg-gray-600 text-white px-3 py-2 mt-2 rounded hover:text-blue-600">
                    <a href="/">Login</a>
                  </li>
                )}
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar2;
