import axios from "axios";
import React from "react";
import Swal from "sweetalert2";

const RecapMasuk = ({ masuk }) => {
  const deleteAbsen = async (id) => {
    Swal.fire({
      title: "Apakah Ingin Di Hapus?",
      text: "Data kamu tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:4000/masuk/" + id);
        Swal.fire({
          icon: "success",
          title: "Berhasil menghapus data",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  return (
    <div>
      <div className="container mx-auto mt-8 ">
        <div className="">
          <div className="flex flex-col">
            <h1 className="text-[24px] ml-[1%] mb-2">Recap Absen</h1>
            <div className="overflow-x-auto">
              <div className=" w-full inline-block align-middle">
                <div className=" border rounded-lg">
                  <table className="min-w-full  divide-y divide-gray-200 ">
                    <thead className="bg-gray-50">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Ket
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                         Jam
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Tanggal
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-right text-gray-500 uppercase "
                        >
                          Action
                        </th>
                      </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200 bg-gray-200">
                      
                      {masuk.length !== 0 ? (
                        <>
                          {masuk.map((masuk) => {
                            return (
                              <tr key={masuk.id}>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {masuk.keterangan}
                                </td>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {masuk.masuk}
                                </td>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {masuk.tanggal}
                                </td>

                                <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                  <a
                                    className="text-red-500 hover:text-red-700 cursor-pointer"
                                    onClick={() => deleteAbsen(masuk.id)}
                                  >
                                    <i class="fa-solid fa-trash-can text-xl pr-4"></i>
                                  </a>
                                </td>
                              </tr>
                            );
                          })}
                        </>
                      ) : (
                        <tr>
                          <td colSpan={4} className="max-md:ml-[-200px]">
                            <center>
                              <h4 className="my-[10%] text-[24px] text-bold">
                                Tidak ada item
                              </h4>
                            </center>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RecapMasuk;
