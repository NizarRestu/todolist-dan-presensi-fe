import axios from "axios";
import React, { useEffect, useState } from "react";
import Navbar from "../Component/Navbar";
import RecapMasuk from "./Component/RecapMasuk";
import Pagination from "../Component/Pagination";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";
const Presensi = () => {
  // State
  const [masuk, setMasuk] = useState([]);
  const history = useHistory();

  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage] = useState(4);

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
  const currentRecords = masuk.slice(indexOfFirstRecord, indexOfLastRecord);
  const nPages = Math.ceil(masuk.length / recordsPerPage);

  // End State

  // Method
  const getAll = async () => {
    await axios
      .get(
        "http://localhost:4000/masuk?user_id=" + localStorage.getItem("userId")
      )
      .then((res) => {
        setMasuk(
          res.data.data.map((e, i) => ({
            ...e,
            no: i + 1,
          }))
        );
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  useEffect(() => {
    getAll();
  }, []);
  const absenmasuk = async (e) => {
    e.preventDefault();
    await axios.post("http://localhost:4000/masuk/absen-masuk", {
      keterangan: "MASUK",
      userId: localStorage.getItem("userId"),
    });
    localStorage.setItem("Status", "Sudah");
    history.push("/presensi");
    Swal.fire({
      icon: "success",
      title: "Berhasil Absen",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  };
  const absen = async (e) => {
    e.preventDefault();
    await axios.post("http://localhost:4000/masuk/absen-pulang", {
      keterangan: "PULANG",
      userId: localStorage.getItem("userId"),
    });
    localStorage.setItem("Status Pulang", "Sudah");
    history.push("/presensi");
    Swal.fire({
      icon: "success",
      title: "Berhasil Absen",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  };
  // End Method
  return (
    <div>
      {/* Presensi */}
      <div className=" ">
        {/* Tombol Absen */}
        <div className="flex pt-[5%]">
          <Navbar />
          <div className="container mx-auto mt-[20%] lg:mt-12 lg:ml-[100px] p-4">
            <div className="grid lg:ml-28 grid-cols-1 gap-12 mb-6 lg:grid-cols-3">
              {localStorage.getItem("Status") !== null ? (
                <a>
                  <div className="w-full px-4 py-5 bg-white rounded-lg shadow">
                    <div className="text-sm font-medium text-gray-500 truncate">
                      Absen Masuk
                    </div>
                    <div className="mt-1 text-3xl font-semibold text-gray-900">
                      {localStorage.getItem("Status") === null ? (
                        <>Belum</>
                      ) : (
                        <> Sudah</>
                      )}
                    </div>
                  </div>
                </a>
              ) : (
                <a onClick={absenmasuk}>
                  <div className="w-full px-4 py-5 bg-white rounded-lg shadow">
                    <div className="text-sm font-medium text-gray-500 truncate">
                      Absen Masuk
                    </div>
                    <div className="mt-1 text-3xl font-semibold text-gray-900">
                      {localStorage.getItem("Status") === null ? (
                        <>Belum</>
                      ) : (
                        <> Sudah</>
                      )}
                    </div>
                  </div>
                </a>
              )}
              {localStorage.getItem("Status Pulang") !== null ? (
                <a>
                  <div className="w-full px-4 py-5 bg-white rounded-lg shadow">
                    <div className="text-sm font-medium text-gray-500 truncate">
                      Absen Pulang
                    </div>
                    <div className="mt-1 text-3xl font-semibold text-gray-900">
                      {localStorage.getItem("Status Pulang") === null ? (
                        <>Belum</>
                      ) : (
                        <> Sudah</>
                      )}
                    </div>
                  </div>
                </a>
              ) : (
                <a onClick={absen}>
                  <div className="w-full px-4 py-5 bg-white rounded-lg shadow">
                    <div className="text-sm font-medium text-gray-500 truncate">
                      Absen Pulang
                    </div>
                    <div className="mt-1 text-3xl font-semibold text-gray-900">
                      {localStorage.getItem("Status Pulang") === null ? (
                        <>Belum</>
                      ) : (
                        <> Sudah</>
                      )}
                    </div>
                  </div>
                </a>
              )}
            </div>
          </div>
        </div>
        {/* End Tombol */}

        {/* Table Absen */}
        <div className="">
          <RecapMasuk masuk={currentRecords} />
        </div>
        {/* End Table Absen */}

        {/* Pagination */}
        {masuk.length > 4 ? (
          <Pagination
            nPages={nPages}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        ) : (
          <></>
        )}
        {/* End Pagination */}

        {/* Footer */}
        <footer className="p-4  sm:p-6 bg-gray-300 mt-[10%] dark:bg-gray-900">
          <div className="md:flex md:justify-between">
            <div className="mb-6 md:mb-0">
              <a href="/home" className="flex items-center">
                <img
                  src="https://to-do-cdn.microsoft.com/static-assets/c87265a87f887380a04cf21925a56539b29364b51ae53e089c3ee2b2180148c6/icons/logo.png"
                  className="h-8 mr-3"
                  alt="FlowBite Logo"
                />
                <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">
                  TodoList
                </span>
              </a>
            </div>
            <div className="grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-">
              <div>
                <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">
                  Resources
                </h2>
                <ul className="text-gray-600 dark:text-gray-400">
                  <li className="mb-4">
                    <a
                      href="https://reactjs.org/"
                      target="_blank"
                      className="hover:underline"
                    >
                      React Js
                    </a>
                  </li>
                  <li>
                    <a
                      href="https://tailwindcss.com/"
                      target="_blank"
                      className="hover:underline"
                    >
                      Tailwind CSS
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <hr className="my-6 border-gray-500 sm:mx-auto dark:border-gray-700 lg:my-8" />
          <div className="sm:flex sm:items-center sm:justify-between">
            <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
              © 2023{" "}
              <a href="/" target="_blank" className="hover:underline">
                TodoList™
              </a>
              . All Rights Reserved.
            </span>
          </div>
        </footer>
        {/* End Footer */}
      </div>
      {/* End Presensi */}
    </div>
  );
};

export default Presensi;
