import axios from "axios";
import React, { useEffect, useState } from "react";
import Navbar from "../Component/Navbar";

const TodoList = () => {
  // State 
  const [plan, setPlan] = useState([]);
  const [task, setTask] = useState([]);
  const [selesai, setSelesai] = useState([]);
  // End State

  // Method
  const getAll = async () => {
    await axios
      .get(
        "http://localhost:4000/plan?user_id=" + localStorage.getItem("userId")
      )
      .then((res) => {
        setPlan(
          res.data.data.map((e, i) => ({
            ...e,
            no: i + 1,
          }))
        );
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  const getAll2 = async () => {
    await axios
      .get(
        "http://localhost:4000/task?user_id=" + localStorage.getItem("userId")
      )
      .then((res) => {
        setTask(
          res.data.data.map((e, i) => ({
            ...e,
            no: i + 1,
          }))
        );
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  const getAll3 = async () => {
    await axios
      .get(
        "http://localhost:4000/finish?user_id=" + localStorage.getItem("userId")
      )
      .then((res) => {
        setSelesai(
          res.data.data.map((e, i) => ({
            ...e,
            no: i + 1,
          }))
        );
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  useEffect(() => {
    getAll();
    getAll2();
    getAll3();
  }, []);
  // End Method
  return (
    <div className=" ">
      {/* Todolist */}
      <div className="flex   pt-[5%]">
      {/* Navbar */}
        <Navbar />
        {/* End Navbar */}
        {/*Tombol todolist  */}
        <div className="container lg:mx-auto mt-[20%] lg:mt-12 lg:ml-[-30px] p-4">
          <div className="grid lg:ml-28 grid-cols-1 gap-12 mb-6 lg:grid-cols-3">
            <a href="/plan">
              <div className="w-full px-4 py-5 bg-white rounded-lg shadow">
                <div className="text-sm font-medium text-gray-500 truncate">
                  Plan Project
                </div>
                <div className="mt-1 text-3xl font-semibold text-gray-900">
                  {plan.length}
                </div>
              </div>
            </a>
            <a href="/task">
              <div className="w-full px-4 py-5 bg-white rounded-lg shadow">
                <div className="text-sm font-medium text-gray-500 truncate">
                  Task
                </div>
                <div className="mt-1 text-3xl font-semibold text-gray-900">
                  {task.length}
                </div>
              </div>
            </a>
            <a href="/selesai">
              <div className="w-full px-4 py-5 bg-white rounded-lg shadow">
                <div className="text-sm font-medium text-gray-500 truncate">
                  Task Selesai
                </div>
                <div className="mt-1 text-3xl font-semibold text-gray-900">
                {selesai.length}
                </div>
              </div>
            </a>
          </div>
        </div>
        {/* End Tombol Todolist */}
      </div>
      {/* End Todolist */}

      {/* Footer */}
      <footer className="p-4  sm:p-6 bg-gray-300 mt-[10%] dark:bg-gray-900">
        <div className="md:flex md:justify-between">
          <div className="mb-6 md:mb-0">
            <a href="/home" className="flex items-center">
              <img
                src="https://to-do-cdn.microsoft.com/static-assets/c87265a87f887380a04cf21925a56539b29364b51ae53e089c3ee2b2180148c6/icons/logo.png"
                className="h-8 mr-3"
                alt="FlowBite Logo"
              />
              <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">
                TodoList
              </span>
            </a>
          </div>
          <div className="grid grid-cols-2 gap-8 sm:gap-6 sm:grid-cols-">
            <div>
              <h2 className="mb-6 text-sm font-semibold text-gray-900 uppercase dark:text-white">
                Resources
              </h2>
              <ul className="text-gray-600 dark:text-gray-400">
                <li className="mb-4">
                  <a
                    href="https://reactjs.org/"
                    target="_blank"
                    className="hover:underline"
                  >
                    React Js
                  </a>
                </li>
                <li>
                  <a
                    href="https://tailwindcss.com/"
                    target="_blank"
                    className="hover:underline"
                  >
                    Tailwind CSS
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <hr className="my-6 border-gray-500 sm:mx-auto dark:border-gray-700 lg:my-8" />
        <div className="sm:flex sm:items-center sm:justify-between">
          <span className="text-sm text-gray-500 sm:text-center dark:text-gray-400">
            © 2023{" "}
            <a href="/" target="_blank" className="hover:underline">
              TodoList™
            </a>
            . All Rights Reserved.
          </span>
        </div>
      </footer>
      {/* End Footer */}
    </div>
  );
};

export default TodoList;
