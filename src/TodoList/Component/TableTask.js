import axios from "axios";
import React from "react";
import Swal from "sweetalert2";

const TableTask = ({ task }) => {
  const deleteTask = async (id) => {
    Swal.fire({
      title: "Apakah Ingin Di Hapus?",
      text: "Data kamu tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:4000/task/" + id);
        axios.delete("http://localhost:4000/finish/" + id);
        Swal.fire({
          icon: "success",
          title: "Berhasil menghapus data",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  const checklistTask = async (id) => {
    Swal.fire({
      title: "Apakah Sudah Selesai?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Sudah!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.put("http://localhost:4000/finish/checklist/" + id, {
          keterangan: "SELESAI",
          userId: localStorage.getItem("userId"),
        });
        Swal.fire({
          icon: "success",
          title: "Berhasil",
          showConfirmButton: false,
          timer: 1500,
        });
        axios.delete("http://localhost:4000/task/" + id);
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  return (
    <div>
      <div className="container lg:mx-auto ">
        <div className="bg-gray-300 lg:px-4 ">
          <div className="flex flex-col">
            <div className="overflow-x-auto">
              <div className="p-1.5 w-full inline-block align-middle">
                <div className=" border rounded-lg">
                  <table className="min-w-full divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          No
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Task
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Jam Mulai
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Jam Akhir
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Detail Task
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Edit
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-right text-gray-500 uppercase "
                        >
                          Hapus
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-right text-gray-500 uppercase "
                        >
                          Checklist
                        </th>
                      </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200 bg-gray-200">
                      {task.length !== 0 ? (
                        <>
                          {task.map((task) => {
                            return (
                              <tr key={task.id}>
                                <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                                  {task.no}
                                </td>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {task.nama}
                                </td>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {task.jamMulai}
                                </td>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {task.jamAkhir}
                                </td>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {task.detail}
                                </td>
                                <td className="px-6 py-4 text-sm font-medium  whitespace-nowrap">
                                  <a
                                    className="text-green-500 hover:text-green-700 cursor-pointer"
                                    href={"/edit-task/" + task.id}
                                  >
                                   <i class="fa-solid fa-pen-to-square text-xl"></i>
                                  </a>
                                </td>

                                <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                  <a
                                    onClick={() => deleteTask(task.id)}
                                    className="text-red-500 hover:text-red-700 cursor-pointer"
                                  >
                                    <i class="fa-solid fa-trash-can text-xl pr-4"></i>
                                  </a>
                                </td>
                                <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                  <a className="text-green-500 hover:text-green-700 cursor-pointer"
                                  onClick={() => checklistTask(task.id)}>
                                    
                                    <i className="fa-solid fa-check text-xl pr-4"></i>
                                  </a>
                                </td>
                              </tr>
                            );
                          })}
                        </>
                      ) : (
                        <tr>
                          <td colSpan={8} className="">
                            <center>
                              <h4 className="my-[10%] text-[24px] text-bold">
                                Tidak ada item
                              </h4>
                            </center>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TableTask;
