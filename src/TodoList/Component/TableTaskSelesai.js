import React from "react";
import Swal from "sweetalert2";
import axios from "axios";
const TableTaskSelesai = ({ selesai }) => {
  // Method
  const deleteTask = async (id) => {
    Swal.fire({
      title: "Apakah Ingin Di Hapus?",
      text: "Data kamu tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:4000/finish/" + id);
        Swal.fire({
          icon: "success",
          title: "Berhasil menghapus data",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  // End Method
  return (
    // Table Task Selesai
    <div>
      <div className="container lg:mx-auto">
        <div className="bg-gray-300 lg:px-4 ">
          <div className="flex flex-col">
            <div className="overflow-x-auto">
              <div className="p-1.5 w-full inline-block align-middle">
                <div className=" border rounded-lg mt-24">
                  <table className="min-w-full divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          No
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          KET
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Jam
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Tanggal
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Nama Task
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-right text-gray-500 uppercase "
                        >
                          Hapus
                        </th>
                      </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200 bg-gray-200">
                      {selesai.length !== 0 ? (
                        <>
                          {selesai.map((selesai) => {
                            return (
                              <>
                                {selesai.keterangan === "SELESAI" ? (
                                  <tr key={selesai.id}>
                                    <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                                      {selesai.no}
                                    </td>
                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                      {selesai.keterangan}
                                    </td>
                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                      {selesai.masuk}
                                    </td>
                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                      {selesai.tanggal}
                                    </td>
                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                      {selesai.nama}
                                    </td>
                                    <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                      <a
                                        onClick={() => deleteTask(selesai.id)}
                                        className="text-red-500 hover:text-red-700 cursor-pointer"
                                      >
                                         <i class="fa-solid fa-trash-can text-xl pr-4"></i>
                                      </a>
                                    </td>
                                  </tr>
                                ) : (
                                  <tr className="hidden" key={selesai.id}>
                                    <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                                      {selesai.no}
                                    </td>
                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                      {selesai.keterangan}
                                    </td>
                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                      {selesai.masuk}
                                    </td>
                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                      {selesai.tanggal}
                                    </td>
                                    <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                      {selesai.nama}
                                    </td>
                                    <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                      <a
                                        onClick={() => deleteTask(selesai.id)}
                                        className="text-red-500 hover:text-red-700 cursor-pointer"
                                      >
                                        Hapus
                                      </a>
                                    </td>
                                  </tr>
                                )}
                              </>
                            );
                          })}
                        </>
                      ) : (
                        <tr>
                          <td colSpan={8} className="">
                            <center>
                              <h4 className="my-[10%] text-[24px] text-bold">
                                Tidak ada item
                              </h4>
                            </center>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    // End Table Task Seleasi
  );
};

export default TableTaskSelesai;
