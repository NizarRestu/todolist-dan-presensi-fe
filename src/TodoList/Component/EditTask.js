import axios from "axios";
import React, { useState, useEffect } from "react";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import { useHistory, useParams } from "react-router-dom";
import Swal from "sweetalert2";
const EditTask = () => {
    const param = useParams();
    const [nama, setNama] = useState("");
    const [detail ,setDetail] = useState("");
    const[jamMulai , setJamMulai] = useState("");
    const[jamAkhir , setJamAkhir] = useState("");
    const history = useHistory();

    useEffect(() => {
        // library opensource yang digunakan untuk request data melalui http.
        axios
          .get("http://localhost:4000/task/" + param.id)
          .then((response) => {
            const newList = response.data.data;
            setNama(newList.nama);
            setDetail(newList.detail);
            setJamMulai(newList.jamMulai);
            setJamAkhir(newList.jamAkhir);
          })
          .catch((error) => {
            alert("Terjadi Kesalahan " + error);
          });
      }, []);
      const updateList = async (e) => {
        e.preventDefault();
        Swal.fire({
          title: "Apakah Ingin Di Update?",
          text: "Data kamu yang lama tidak bisa dikembalikan!",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Ya, Di Update!",
        }).then((result) => {
          if (result.isConfirmed) {
            axios.put(
              "http://localhost:4000/task/" + param.id,{
                nama:nama,
                detail:detail,
                jamMulai:jamMulai,
                jamAkhir:jamAkhir,
                userId: localStorage.getItem("userId"),
            }
            );
            axios.put(
              "http://localhost:4000/finish/" + param.id,{
                nama:nama,
                userId:localStorage.getItem("userId")
              }
            )
            setTimeout(() => {
              window.location.reload();
              history.push("/task");
              
            }, 1500);
            Swal.fire({
              icon: "success",
              title: "Berhasil mengedit data",
              showConfirmButton: false,
              timer: 1500,
            });
          }
          history.push("/task");
        });
      };
  return (
    <div>
      <div className="flex justifiy-center items-center bg-gray-300 w-full h-screen ">
        <div className="bg-white w-[80%] m-auto h-[80%%]  p-[4%] rounded-[30px]">
          <Form className="" onSubmit={updateList}>
            <h5>Edit List</h5>
            <Form.Group className="mb-3" controlId="formBasicEmail">
              <Form.Label>Nama Task</Form.Label>
              <Form.Control
                type="text"
                required
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </Form.Group>

            <Form.Group className="mb-3" controlId="formBasicPassword">
              <Form.Label>Detail</Form.Label>
              <Form.Control
                required
                type="text"
                value={detail}
                onChange={(e) => setDetail(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Jam Mulai</Form.Label>
              <Form.Control
                required
                type="time"
                value={jamMulai}
                onChange={(e) => setJamMulai(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label htmlFor="judul">Jam Akhir</Form.Label>
              <br />
              <Form.Control
                required
                type="time"
                value={jamAkhir}
                onChange={(e) => setJamAkhir(e.target.value)}
              />
            </Form.Group>

            <a href="/task">
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
              variant="primary"
            >
              Save
            </Button>
            </a>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default EditTask;
