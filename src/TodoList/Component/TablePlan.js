import axios from "axios";
import React from "react";
import Swal from "sweetalert2";

const TablePlan = ({ plan }) => {
  const deletePlan = async (id) => {
    Swal.fire({
      title: "Apakah Ingin Di Hapus?",
      text: "Data kamu tidak bisa dikembalikan!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:4000/plan/" + id);
        Swal.fire({
          icon: "success",
          title: "Berhasil menghapus data",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  const cheklistPlan = async (id) => {
    Swal.fire({
      title: "Apakah Project Anda Sudah Selesai?",
      text: "",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Ya, Hapus!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete("http://localhost:4000/plan/" + id);
        Swal.fire({
          icon: "success",
          title: "Berhasil!!",
          showConfirmButton: false,
          timer: 1500,
        });
        setTimeout(() => {
          window.location.reload();
        }, 1500);
      }
    });
  };
  return (
    <div>
      <div className="container lg:mx-auto">
        <div className="bg-gray-300 lg:px-4 ">
          <div className="flex flex-col">
            <div className="overflow-x-auto">
              <div className="p-1.5 w-full inline-block align-middle">
                <div className=" border rounded-lg">
                  <table className="min-w-full max-md:px-24 divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          No
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Nama Project
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Deskripsi
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-left text-gray-500 uppercase "
                        >
                          Edit
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-right text-gray-500 uppercase "
                        >
                          Hapus
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-xs font-bold text-right text-gray-500 uppercase "
                        >
                          Checklist
                        </th>
                      </tr>
                    </thead>
                    <tbody className="divide-y divide-gray-200 bg-gray-200">
                      {plan.length !== 0 ? (
                        <>
                          {plan.map((plan) => {
                            return (
                              <tr key={plan.id}>
                                <td className="px-6 py-4 text-sm font-medium text-gray-800 whitespace-nowrap">
                                  {plan.no}
                                </td>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {plan.nama}
                                </td>
                                <td className="px-6 py-4 text-sm text-gray-800 whitespace-nowrap">
                                  {plan.deskripsi}
                                </td>
                                <td className="px-6 py-4 text-sm font-medium  whitespace-nowrap">
                                  <a
                                    className="text-green-500 hover:text-green-700 cursor-pointer"
                                    href={"/edit-plan/" + plan.id}
                                  >
                                    <i class="fa-solid fa-pen-to-square text-xl"></i>
                                  </a>
                                </td>

                                <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                  <a
                                    className="text-red-500 hover:text-red-700 cursor-pointer"
                                    onClick={() => deletePlan(plan.id)}
                                  >
                                    <i class="fa-solid fa-trash-can text-xl pr-4"></i>
                                  </a>
                                </td>
                                <td className="px-6 py-4 text-sm font-medium text-right whitespace-nowrap">
                                  <a
                                    className="text-green-500 hover:text-green-700 cursor-pointer"
                                    href="#"
                                    onClick={() => cheklistPlan(plan.id)}
                                  >
                                    <i className="fa-solid fa-check text-xl pr-4"></i>
                                  </a>
                                </td>
                              </tr>
                            );
                          })}
                        </>
                      ) : (
                        <tr>
                          <td colSpan={6} className="max-md:ml-[-200px]">
                            <center>
                              <h4 className="my-[10%] text-[24px] text-bold">
                                Tidak ada item
                              </h4>
                            </center>
                          </td>
                        </tr>
                      )}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TablePlan;
