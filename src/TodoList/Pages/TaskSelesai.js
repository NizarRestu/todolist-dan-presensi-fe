import axios from "axios";
import React, { useEffect, useState } from "react";

import Navbar from "../../Component/Navbar";
import Pagination from "../../Component/Pagination";
import TableTaskSelesai from "../Component/TableTaskSelesai";

const TaskSelesai = () => {
  // State
  const [selesai, setSelesai] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage] = useState(5);

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
  const currentRecords = selesai.slice(indexOfFirstRecord, indexOfLastRecord);
  const nPages = Math.ceil(selesai.length / recordsPerPage);
// End State

// Method
  const getAll3 = async () => {
    await axios
      .get(
        "http://localhost:4000/finish?user_id=" + localStorage.getItem("userId")
      )
      .then((res) => {
        setSelesai(
          res.data.data.map((e, i) => ({
            ...e,
            no: i + 1,
          }))
        );
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  useEffect(() => {
    getAll3();
  }, []);
  // End Method
  return (
    <div>
      <div className="bg-gray-300 h-[100vh] ">
        <Navbar />
        {/* Table task selesai */}
        <div className="">
          <TableTaskSelesai selesai={currentRecords} />
        </div>
        {/* Pagination */}
        {selesai.length > 5 ? (
          <Pagination
            nPages={nPages}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
          />
        ) : (
          <></>
        )}
        {/* End Pagination */}
      </div>
    </div>
  );
};

export default TaskSelesai;
