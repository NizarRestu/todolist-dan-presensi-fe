import axios from "axios";
import React, { useEffect, useState } from "react";

import Swal from "sweetalert2";
import Navbar from "../../Component/Navbar";
import TableTask from "../Component/TableTask";
import Pagination from "../../Component/Pagination";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
const Task = () => {
  // State
  const [nama, setNama] = useState("");
  const [detail, setDetail] = useState("");
  const [jamMulai, setJamMulai] = useState("");
  const [jamAkhir, setJamAkhir] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [task, setTask] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage] = useState(5);

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
  const currentRecords = task.slice(indexOfFirstRecord, indexOfLastRecord);
  const nPages = Math.ceil(task.length / recordsPerPage);
// End State

// Method
  const addTask = async (e) => {
    e.preventDefault();
    await axios.post("http://localhost:4000/task", {
      nama: nama,
      detail: detail,
      jamMulai: jamMulai,
      jamAkhir: jamAkhir,
      userId: localStorage.getItem("userId"),
    });
    await axios.post("http://localhost:4000/finish", {
      nama: nama,
      userId: localStorage.getItem("userId"),
    });
    Swal.fire({
      icon: "success",
      title: "Berhasil menambahkan task",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  };
  const getAll2 = async () => {
    await axios
      .get(
        "http://localhost:4000/task?user_id=" + localStorage.getItem("userId")
      )
      .then((res) => {
        setTask(
          res.data.data.map((e, i) => ({
            ...e,
            no: i + 1,
          }))
        );
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  useEffect(() => {
    getAll2();
  }, []);
  // End Method
  return (
    // Task
    <div className="bg-gray-300 h-[100vh] ">
      {/* Nabar & Tombol Tambah Task */}
      <Navbar />
      <Button
        className="mt-24 lg:ml-28 max-md:ml-4 border-transparent bg-gray-500 hover:bg-gray-700 hover:border-transparent"
        onClick={handleShow}
      >
        Tambah Task
      </Button>
      {/* End Nabar & Tombol Tambah Task */}

      {/* Table task */}
      <div className="">
        <TableTask task={currentRecords} />
      </div>
      {/* End Table task */}

      {/* Pagination */}
      {task.length > 5 ? (
        <Pagination
          nPages={nPages}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
        />
      ) : (
        <></>
      )}
      {/* End Pagination */}

      {/* Modal */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-gray-700 text-white" closeButton>
          <Modal.Title>Tambah Task</Modal.Title>
        </Modal.Header>
        <Form onSubmit={addTask}>
          <Modal.Body>
            <div className="mb-3">
              <Form.Label htmlFor="judul">Nama Task: </Form.Label>
              <br />
              <Form.Control
                placeholder="Nama Task"
                required
                type="text"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <Form.Label htmlFor="judul">Jam Mulai: </Form.Label>
              <br />
              <Form.Control
                placeholder=""
                required
                type="time"
                value={jamMulai}
                onChange={(e) => setJamMulai(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <Form.Label htmlFor="judul">Jam Akhir: </Form.Label>
              <br />
              <Form.Control
                placeholder=""
                required
                type="time"
                value={jamAkhir}
                onChange={(e) => setJamAkhir(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <Form.Label htmlFor="deskripsi">Deskripsi: </Form.Label>
              <br />
              <Form.Control
                placeholder="Deskripsi"
                required
                type="text"
                value={detail}
                onChange={(e) => setDetail(e.target.value)}
              />
            </div>
            <br />
            <Button
              className="mx-1  border-transparent bg-red-600 hover:bg-white hover:border-red-600 hover:text-red-600"
              variant="danger"
              onClick={handleClose}
            >
              Tutup
            </Button>
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
              variant="primary"
            >
              Tambah
            </Button>
          </Modal.Body>
          <Modal.Footer></Modal.Footer>
        </Form>
      </Modal>
      {/* End Modal */}
    </div>
    // End Task
  );
};

export default Task;
