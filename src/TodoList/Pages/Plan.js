import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import Navbar from "../../Component/Navbar";
import TablePlan from "../Component/TablePlan";
import Pagination from "../../Component/Pagination";
import Modal from "react-bootstrap/Modal";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

const Plan = () => {
  // State
  const [nama, setNama] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [plan, setPlan] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordsPerPage] = useState(5);

  const indexOfLastRecord = currentPage * recordsPerPage;
  const indexOfFirstRecord = indexOfLastRecord - recordsPerPage;
  const currentRecords = plan.slice(indexOfFirstRecord, indexOfLastRecord);
  const nPages = Math.ceil(plan.length / recordsPerPage);

  // End State

  // Method
  const getAll = async () => {
    await axios
      .get(
        "http://localhost:4000/plan?user_id=" + localStorage.getItem("userId")
      )
      .then((res) => {
        setPlan(
          res.data.data.map((e, i) => ({
            ...e,
            no: i + 1,
          }))
        );
      })
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };
  useEffect(() => {
    getAll();
  }, []);

  const addProject = async (e) => {
    e.preventDefault();
    await axios.post("http://localhost:4000/plan", {
      nama: nama,
      deskripsi: deskripsi,
      userId: localStorage.getItem("userId"),
    });
    Swal.fire({
      icon: "success",
      title: "Berhasil menambahkan project",
      showConfirmButton: false,
      timer: 1500,
    });
    setTimeout(() => {
      window.location.reload();
    }, 1500);
  };
  // End State
  return (
    <div className="bg-gray-300 h-[100vh]">
      <Navbar />
      {/* Tombol tambah plan */}
      <Button  className="mt-24 lg:ml-28 max-md:ml-4  border-transparent bg-gray-500 hover:bg-gray-700 hover:border-transparent" onClick={handleShow}>
        Tambah Project
      </Button>
      {/* Table Plan */}
      <div className="">
        <TablePlan plan={currentRecords} />
      </div>
      {/* Pagination */}
      {plan.length > 5 ? (
        <Pagination
          nPages={nPages}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
        />
      ) : (
        <></>
      )}
      {/* End Pagination */}

      {/* Modal tambah plan */}
      <Modal show={show} onHide={handleClose}>
        <Modal.Header className="bg-gray-700 text-white" closeButton>
          <Modal.Title>Tambah Project</Modal.Title>
        </Modal.Header>
        <Form onSubmit={addProject}>
          <Modal.Body>
            <div className="mb-3">
              <Form.Label htmlFor="judul">Nama Project: </Form.Label>
              <br />
              <Form.Control
                placeholder="Nama Project"
                required
                type="text"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
              />
            </div>
            <div className="mb-3">
              <Form.Label htmlFor="deskripsi">Deskripsi: </Form.Label>
              <br />
              <Form.Control
                placeholder="Deskripsi"
                required
                type="text"
                value={deskripsi}
                onChange={(e) => setDeskripsi(e.target.value)}
              />
            </div>
            <br />
            <Button
              className="mx-1  border-transparent bg-red-600 hover:bg-white hover:border-red-600 hover:text-red-600"
              variant="danger"
              onClick={handleClose}
            >
              Tutup
            </Button>
            <Button
              className="mx-1 border-transparent bg-blue-600 hover:bg-white hover:border-blue-600 hover:text-blue-600"
              type="submit"
              variant="primary"
            >
              Tambah
            </Button>
          </Modal.Body>
          <Modal.Footer></Modal.Footer>
        </Form>
      </Modal>
      {/* End modal tambah plan */}
    </div>
  );
};

export default Plan;
