import Register from "./Pages/Register";
import Login from "./Pages/Login";
import Home from "./Pages/Home";
import TodoList from "./TodoList/TodoList";
import Plan from "./TodoList/Pages/Plan";
import Task from "./TodoList/Pages/Task";
import TaskSelesai from "./TodoList/Pages/TaskSelesai";
import Presensi from "./Presensi/Presensi";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import EditTask from "./TodoList/Component/EditTask";
import EditPlan from "./TodoList/Component/EditPlan";
import Profile from "./Pages/Profile";
import EditProfile  from "./Pages/EditProfile";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/reg" component={Register} exact />
            <Route path="/" component={Login} exact />
            <Route path="/home" component={Home} exact />
            <Route path="/todolist" component={TodoList} exact />
            <Route path="/plan" component={Plan} exact />
            <Route path="/task" component={Task} exact />
            <Route path="/selesai" component={TaskSelesai} exact />
            <Route path="/presensi" component={Presensi} exact />
            <Route path="/edit-task/:id" component={EditTask} exact/>
            <Route path="/edit-plan/:id" component={EditPlan} exact/>
            <Route path="/profile" component={Profile} exact/>
            <Route path="/edit-profile" component={EditProfile} exact/>
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
